# Balder OS

A respin of Alpine OS for my own purpose.


## Installation

The installation.sh script will do this instllation step for you.

  0. Have a Alpine installation ready and connectet to internet, and you have a computer ready to to installations tasks.  
  1. Downlod basic-setup.sh and run it. Like: "wget basic-setup.sh; bash -"
  2. Go to directory dev/balder-os in your home direktory. 
  3. Run personal-server.sh script.
  4. If you want admin server run admin-server.sh.
  5. If you want desktop run playbook descktop.sh.
  6. Save your system changes with alpine backup tools.
  7. Log into you OS.

## Basic setup
  * ssh - for secure comunication
  * sudo - So user can be sudo
  * "Create a user, default is balder, meber of group sudo"

## private server

### Core Apps
  * neovim - for edtiting
  * nginx - for reverse proxy and https.
  * git - for version controlling.
  * encfs - for crypt.
  * docker 

### Docker Container
  * nextcloud-client  - A client for owncloud.https://hub.docker.com/r/toughiq/owncloud-client/ 

## admin servers
### Core Apps
  * kvm -
  * virtualbox-headless - 
 
### Docker Container
  * ttyd - Terminal over web. https://github.com/tsl0922/ttyd/blob/master/Dockerfile

## desktop
### Core Apps
  * xfree - Basic grafik


## Adition
### Docker Container
  * minecraft-server - A minecraft serve. https://github.com/itzg/dockerfiles/tree/master/minecraft-server
  * weechat - Client for irc mm. https://hub.docker.com/r/jkaberg/weechat/ 
