" My vimrc config

" For all
"
set nocompatible

" Colors

syntax enable           " enable syntax processing

" Show Code

set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
set showmatch           " highlight matching [{()}]

" Menus

set wildmenu            " visual autocomplete for command menu

" Tabs

set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces

" Shortcuts

inoremap jk <esc>   " jk is escap

" From http://stackoverflow.com/questions/327411/how-do-you-prefer-to-switch-between-buffers-in-vim
" Tab triggers buffer-name auto-completion

set wildchar=<Tab> wildmenu wildmode=full
let mapleader = "f"

map <Leader>p :bprev<Return>
map <Leader>n :bnext<Return>
map <Leader>d :bd<Return>
map <Leader>l :ls<Return>
map <Leader>f :b#<Return>

set hidden

" For swedish layoute

:nmap ö :
:nmap ä $

" Backups

" From http://stackoverflow.com/questions/9987887/vimrc-when-backupdir-is-set-every-save-prompts-press-enter-to-continue
if !isdirectory(expand("~/.vim/backupdir/"))
        silent !echo "Creating backup dir..."
        silent !mkdir -p ~/.vim/backup
endif
if !isdirectory(expand("~/.vim/swap/"))
        silent !echo "Creating swap dir..."
        silent !mkdir -p ~/.vim/swap
endif

if !isdirectory(expand("~/.vim/undo/"))
        silent !echo "Creating undo dir..."
        silent !mkdir -p ~/.vim/undo
endif

set backupdir^=~/.vim/backup//              " where to put backup files
set directory^=~/.vim/swap//                " where to put swap files
set undodir^=~/.vim/undo//

set backup
set writebackup

" Plugins

" Required
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'ctrlpvim/ctrlp.vim'

call vundle#end()
filetype plugin indent on

