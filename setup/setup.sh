#!/bin/bash
# Author: Magnus Kronnäs

# Reimplemnt scripts from lib/volor-and-style.sh

TC='\e['
Rst="${TC}0m"     

Bold="${TC}1m"    
Undr="${TC}4m"    

Black="${TC}30m"; 
Red="${TC}31m"; 
Green="${TC}32m"; 
Yellow="${TC}33m";
Blue="${TC}34m";  
Purple="${TC}35m";
Cyan="${TC}36m";
White="${TC}37m"; 

# Reimplemnt scripts from lib/common.sh

BALDER_COMMON=1;
MESSAGE_TIME=1;

# For give time to user to reflect
msgHeadline(){
        echo -e $MESSAGE_TIME;
        sleep $MESSAGE_TIME;
};

# Print an OK message
msg() {
        echo -e $Green$1$Rst;
};

# Print an informativ message
msgInfo() {
        echo -e $Yellow$1$Rst;
};

# Give a default value to a variable, Make it possible to have flexible 
# configurations.
default() {
   if [ -z ${!1} ]; then 
       eval $1=\$2;
       msgInfo "$1: $2";
   fi
}

# Config
default BALDER_USER "balder";

msgHeadline "Installing basic apps for Balder OS";

msg "Update and upgrade os"
apk update;
apk upgrade;

msgHeadline "Installning programs"

msg "Installing ssh server";
apk add openssh;

msg "Installing ssh client";
apk add openssh-client;

msg "Installing htop";
apk add htop;

msg "Installing git";
apk add git;

msg "Installing sudo command";
apk add sudo;

msg "Create group sudo"
addgroup sudo;

msg "Give sudo rigths to sudo group"
msg '%sudo ALL=(ALL:ALL) ALL' | EDITOR='tee -a' visudo;

msg "Create group fuse"
addgroup fuse;

msg "Maked /dev/fuse usable by fuse  group"
chgrp fuse /dev/fuse;

msg "Change group permission for /dev/fuse"
chmod g+rw /dev/fuse;

msgHeadline "Create a user"

msg "Create user balder"
adduser $BALDER_USER;

msg "Make balder member of sudo group"
adduser $BALDER_USER sudo;

msg "Make balder member of sudo group"
adduser $BALDER_USER fuse;


msg "Prepaer for and install docker"

msg "Add community repository"
echo "http://nl.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories;

msg "Read in community repository."
apk update;

msg "Installing docker"
apk add docker;

msg "Start docker deamon, so we can use docker"
service docker start;

msg "Install some app frpm community"
sleep $MESSAGE_TIME;

msg "Installing neovim editor";
apk add neovim;

msgHeadline "Creating usefull folders"

msg "Create dev folder"
mkdir ~/dev;

msg "Create bin folder for executable files"
mkdir ~/bin;

msg "Create cloud folder for cloud data"
mkdir ~/cloud;

msg "Create encrypt folder for crypted files"
mkdir ~/cloud/encrypt;

msg "Create crypt folder for reading crypted file"
mkdir ~/crypt;

msgHeadline "Balder OS"
cd ~/dev

msgHeadline "Get balder OS repo";
git clone https://gitlab.com/krmit/balder-os-pre.git;
~/dev/balder-os-pre/tools/install-config;

