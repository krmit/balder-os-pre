# Author: Magnus Kronnäs
---
tasks:
  - name: Installing program for a personal server.
  - name: Updating the system.
    apk:
      update_cache: yes
      upgrade: yes
    notify:
      - Install vim
      - Install git
handlers:
  - name: Install vim
    apk:
      name: vim
  - name: Install git
    apk:
      name: git