"use strict"
/*
*   Copyright 2017 Magnus Kronnäs
*
*   This file is part of easy-web-framework.
*
*   mimer is free software: you can redistribute it and/or modify it under the
*   terms of the GNU General Public License as published by the Free Software 
*   Foundation, either version 3 of the License, or (at your option) any later 
*    version.
*
*   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY 
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
*   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along with 
*   easy-web-framework. If not, see http://www.gnu.org/licenses/.
*/

/* eslint no-console: "off", no-unused-vars: "off" */

const colors = require("colors");
const jsome = require("jsome");
const sprintf = require("sprintf-js").sprintf;


function log(level) {
	this._logLevel=level;
	
    if(this._verbose === true)) {
        this._logLevel=5;
    }

    if(this._debug)) {
        this._logLevel = 6;
    }

    if(this._trace) {
        this._logLevel = 7;
    }

    if(this._silent) {
        this._logLevel = 1;
    }

    level = this._logLevel;

    const log_print = function(msg, option) {
        let tag_color = function(msg){return msg;};
        let console_method=console.log;
        let origin_method=function(){return "";};
        switch(option.tag) {
        case "fatal":
        case "error":
            tag_color = colors.red;
            console_method=console.error;
            break;
        case "warn":
            tag_color = colors.yellow;
            break;
        default:
            tag_color = colors.green;
        }
        if(options !== undefined) {
        switch(option.type) {
        case "fatal":
        case "error":
            tag_color = colors.red;
            console_method=console.error;
            break;
        case "warn":
            tag_color = colors.yellow;
            break;
        default:
            tag_color = colors.green;
        }
	    }
        if(option.origin===true) {
            origin_method=getSourceLocation;
        }

        
        if(option.code === true) {
            jsome(msg);
        }
        else {
			tag=sprintf("%-5s",tag);
            console_method(sprintf("[%s] %s %s",tag_color(tag), origin_method(), msg));
		}
    };

    this.trace = function(msg, option) {return this;};
    if(level > 6) {
		option.tag="trace";
        this.trace = function (msg, code) {log_print(msg,option);return this;};
    }

    this.debug = function(msg, code) {return this;};
    if(level > 5) {
        this.debug = function (msg, code) {log_print("debug", msg, code);return this;};
    }

    this.info = function(msg, code) {return this;};
    if(level > 4) {
        this.info = function (msg, code) {log_print("info", msg, code);return this;};
    }

    this.msg = function(msg, code) {return this;};
    if(level > 3) {
        this.msg = function (msg, code) {console.log(msg);return this;};
    }

    this.jsonMsg = function(msg, code) {return this;};
    if(level > 3) {
        this.jsonMsg = function (msg) {jsome(msg);return this;};
    }

    this.warn = function(msg, code) {return this;};
    if(level > 2) {
        this.warn = function (msg, code) {log_print("warn", msg);return this;};
    }

    this.error = function(msg, code) {return this;};
    if(level > 1) {
        this.error = function (msg, code) {log_print("error", msg);return this;};
    }

    this.fatal = function(msg, code) {return this;};
    if(level > 0) {
        this.fatal = function (msg, code) {log_print("fatal", msg);return this;};
    }

    return this;
}

function getSourceLocation() {
    const caller_line = (new Error).stack.split("\n")[4];
    const index = caller_line.indexOf("at ");
    const clean = caller_line.slice(index+2, caller_line.length);
    const number = clean.split(":")[1];
    const fileName=clean.split(" ")[1];
    return sprintf("%-30s",sprintf("%-25s:%s",fileName,number));
}

