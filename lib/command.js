"use strict"

const colors = require("colors");
const systemInstall = require("system-install");
const shell = require('shelljs');

module.exports = function(param) {
	let installer = systemInstall();
	if(installer === "sudo apt-get install") {
		installer+=" -y ";
	}

    let name="";
	if(param.join !== undefined) {
	    name=name.join(" ");
    }
    else {
		name=param;
	}
	
    const command = installer+name;
    console.log(command.red);
    
    return shell.exec(command);
}
